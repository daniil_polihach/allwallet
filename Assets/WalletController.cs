using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using WalletConnectSharp.Unity;

public class WalletController : MonoBehaviour
{

    [SerializeField] Button QRScan_Button;
    [SerializeField] Button WebConnect_Button;

    void Start()
    {
        QRScan_Button.onClick.AddListener(QRWallet);
        WebConnect_Button.onClick.AddListener(WebWallet);
    }


    private void QRWallet()
    {
        SceneManager.LoadScene("LoginScene");
    }
   
    private void WebWallet()
    {
        WalletConnector connector = new WalletConnector();
        connector.Connect((str) => { Debug.Log("Connect " + str); });
    }
}
