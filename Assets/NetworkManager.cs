using Ejaw.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    private string _url = "https://constructionorg.ru/rebel-bots";
    private Connector _connector;

    private void Start()
    {
        _connector = new Connector(_url, true);
        _connector.RegisterHandler<string, string>("ReceiveMessage",
            (arg1, arg2) => { Debug.Log($"{arg1}: {arg2}"); });
        _connector.Connect();

    }

    public void SendMessage()
    {
        _connector.Send("SendMessage", new object[] { "1", "2" });
    }

    public void Disconnect()
    {
        _connector.Disconnect();
    }

    public void Connect()
    {
        _connector.Connect();
    }
}
