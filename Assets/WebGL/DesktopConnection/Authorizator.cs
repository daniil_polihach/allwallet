using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Diagnostics;
using EJaw.Networking.Websocket;
using System.IO;
using EJaw.Networking.CryptoWallet.ServerSide;

namespace EJaw.Desktop.Login
{
    public class Authorizator : MonoBehaviour
    {
        [SerializeField] private GameObject _blocker;
        [SerializeField] private Button _loginButton;
        [SerializeField] private Text _loginStatus;

        private Client _client;
        private HttpServer _server;

        private void Awake()
        {
            _client = new Client();
        }

        private void Start()
        {
            _client.Start();
        }

        private void OnEnable()
        {
            _loginButton.onClick.AddListener(OnLoginButtonClick);
            //_client.MessageGot += OnMessageGot;
        }

        private void OnDisable()
        {
            _loginButton.onClick.RemoveListener(OnLoginButtonClick);
            //_client.MessageGot -= OnMessageGot;
            _client.Dispose();
        }

        private void Update()
        {
            DoUpdate();
        }

        private void OnLoginButtonClick()
        {
            OpenUrl();
        }

        private void OpenUrl()
        {
            string startingPath = Path.Combine(Application.streamingAssetsPath, "WebGL");

            _server = new HttpServer(startingPath, 8004);
            _server.Start();

            Process proc = new Process();
            proc.StartInfo.FileName = @"chrome";
            proc.StartInfo.Arguments = $"http://localhost:8004/ --new-window";
            proc.Start();
        }

        private void OnMessageGot(string message)
        {
            if (string.IsNullOrEmpty(message) == false)
            {
                if (message == "Connected")
                {
                    OnLoginSuccess(message);
                }
            }
        }

        private void DoUpdate()
        {
            lock (_client.Queue)
            {
                if (_client != null && _client.Queue.Any())
                {
                    var message = _client.Queue.Dequeue();
                    OnLoginSuccess(message);
                }
            }
        }

        private void OnLoginSuccess(string message)
        {
            _blocker.gameObject.SetActive(false);
            _loginStatus.text = message;
        }
    }
}


