using EJaw.Networking.Websocket;

namespace EJaw.Desktop.Login
{
    public class ServersController 
    {
        private Server _server;

        public void Start()
        {
            _server = new Server();
            _server.Start();
        }

        public void Stop()
        {
            if (_server != null)
            {
                _server.Dispose();
            }
        }
    }
}


