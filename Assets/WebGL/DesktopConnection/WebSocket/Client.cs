using System.Collections.Generic;
using WebSocketSharp;

namespace EJaw.Networking.Websocket
{
    public class Client 
    {
        public Queue<string> Queue = new Queue<string>();

        private const string _Url = "ws://localhost:5001/Login";
        private WebSocket _webSocket;

        public void Dispose()
        {
            if (_webSocket != null)
            {
                _webSocket.CloseAsync();
            }
        }

        public void Start()
        {
            _webSocket = new WebSocket(_Url);
            _webSocket.OnMessage += OnMessage;

            _webSocket.ConnectAsync();
        }

        public void SendMessage(string message)
        {
            _webSocket.SendAsync(message, OnSended);
        }

        private void OnSended(bool isCompleted)
        {
        }

        private void OnMessage(object sender, MessageEventArgs arguments)
        {
            lock (Queue)
            {
                Queue.Enqueue(arguments.Data);
            }
        }
    }
}


