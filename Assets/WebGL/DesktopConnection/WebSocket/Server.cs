using EJaw.Networking.Websocket.Services;
using System.Collections.Generic;
using WebSocketSharp.Server;

namespace EJaw.Networking.Websocket
{
    public class Server
    {
        public Queue<string> Queue = new Queue<string>();

        private const string _Url = "ws://localhost:5001";
        private WebSocketServer _webSocketServer;

        public void Dispose()
        {
            if (_webSocketServer != null)
            {
                _webSocketServer.Stop();
            }
        }

        public void Start()
        {
            _webSocketServer = new WebSocketServer(_Url);
            _webSocketServer.Start();

            _webSocketServer.AddWebSocketService<Login>("/Login");
            SendMessage();
        }

        public void SendMessage()
        {
            _webSocketServer.WebSocketServices.BroadcastAsync("Connected", OnConnected);
        }

        private void OnConnected()
        {
        }
    }
}


